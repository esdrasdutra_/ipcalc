#!/usr/bin/env python
#-*- coding: utf-8 -*-

class CalculoIp(object):
    '''CalculoIp
    Classe criada para abrigar as funções que serão
    utilizadas no projeto da Calculadora IP'''

    def __init__(self,lensm):
        self.lensm = 0
        self.ipadd = [] #lista com IP em binario
        self.nwadd = []
        self.nw_ip = []
        self.bcadd = []
        self.bc_ip = []
        self.smask = []
        self.indiceL = [7,15,23]
        self.get_bin = lambda x, n: format(x, 'b').zfill(n)


##################################### Calcula IP ####################################
    def getIP(self, ip):
        indice = 0
        ipadd_ = "."
        ip_split = ip.split(".")
        while indice < len(ip_split):
            ipBin = self.get_bin(int(ip_split[indice]),8)
            self.ipadd.append(ipBin)
            indice += 1
            ipaddr = ipadd_.join(self.ipadd)
        return (ipaddr)



###########################Submascara################################################
    def getSM(self,lensm):
        indice = 0
        self.lensm = int(lensm)
        while indice < self.lensm:
            self.smask.append("1")
            if (indice in self.indiceL):
                self.smask.append(".")
            indice += 1

        if (indice == self.lensm) and (indice < 31):
            while indice <= 31:
                self.smask.append("0")
                if (indice in self.indiceL):
                    self.smask.append(".")
                indice += 1
        return (''.join(self.smask))


###################################IP Rede###########################################
    def getIpRede(self, a, b):
        ind = 0
        a_sp = a.split(".")
        b_sp = b.split(".")
        while ind < len(a_sp):
            binaryAnd = str(((int(a_sp[ind], 2)) & (int(b_sp[ind], 2))))
            self.nw_ip.append(binaryAnd)
            ind+= 1
            netip = '.'.join(self.nw_ip)

        j = 0
        nw_split =netip.split(".")
        while j < len(nw_split):
            ipBin = self.get_bin(int(nw_split[j]),8)
            self.nwadd.append(ipBin)
            j += 1
            nwaddr= '.'.join(self.nwadd)
        return(nwaddr)



    def reversSM(self, b,c):
        indice = 0
        bc = []
        while indice < self.lensm:
                bc.append('0')
                if (indice in self.indiceL):
                        bc.append('.')
                indice += 1
        if (indice == self.lensm) and (indice < 31):
                while indice <=31:
                        bc.append('1')
                        if (indice in self.indiceL):
                                bc.append('.')
                        indice += 1

        return (''.join(bc))


    def getBroadcast(self, a, c, d):
        ind = 0
        newc = c[0:self.lensm]+d[self.lensm:35]
        b_sp = a.split(".")
        c_sp = newc.split(".")
        while ind < len(b_sp):
            binaryAnd = str(((int(b_sp[ind], 2)) | (int(c_sp[ind], 2))))
            self.bc_ip.append(binaryAnd)
            ind+= 1
            bcip = '.'.join(self.bc_ip)

        j = 0
        bc_split = bcip.split(".")
        while j < len(bc_split):
            ipBin = self.get_bin(int(bc_split[j]),8)
            self.bcadd.append(ipBin)
            j += 1
            bcaddr= '.'.join(self.bcadd)

        return(bcaddr)

    def toDec(self,string):
        numberEx = string
        numberSplit = numberEx.split('.')
        ip = []

        for i in numberSplit:
            ip.append((str(int(i,2))))
        ipJ = '.'.join(ip)

        return(ipJ)

    def getHST(self,string):
        submask = string
        var1 = 0
        for i in submask:
            if i == '0':
                var1 += 1
        return(pow(2,var1)-2)